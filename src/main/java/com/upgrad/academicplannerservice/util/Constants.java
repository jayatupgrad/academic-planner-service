package com.upgrad.academicplannerservice.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    public final String AUTH_TOKEN_HEADER = "auth-token";
    public final String COURSE_ID_HEADER = "course-id";

    @UtilityClass
    public class UrlConstants {
        public final String PLUS = "+";
        public final String COLON = ":";
        public final String HTTP_SCHEME = "http://";
        public final String PLUS_ENCODED_STRING = "%2B";
    }

    @UtilityClass
    public class Profile {
        public final String PRODUCTION = "production";
        public final String TEST = "test";
        public final String LOCAL = "local";
        public final String STAGING = "staging";
        public final String DEV = "dev";
    }

    @UtilityClass
    public class MDCConstants {
        public final String USER_ID = "userId";
        public final String ROLE = "role";
        public final String REQUEST_URL = "requestUrl";
        public final String REQUEST_TYPE = "requestType";
        public final String IP_ADDRESS = "ipAddress";
        public final String EMAIL = "email";
        public final String BROWSER_INFO = "browserInfo";
        public final String PLATFORM = "platform";
        public final String DEVICE_TYPE = "deviceType";
        public final String OPERATING_SYSTEM = "operatingSystem";
        public final String SESSION_ID = "sessionId";
        public final String HEADER_CLIENT_IP = "X-Forwarded-For";
        public final String HEADER_USER_AGENT = "User-Agent";
        public final String AUTH_TOKEN = "Auth-Token";
        public final String REQUEST_BODY = "requestBody";
        public final String TOTAL_TIME = "totalTime";
        public final String STATUS_CODE = "statusCode";
        public final String ERROR_MESSAGE = "errorMessage";

        public final String DEFAULT_STRING = "ANONYMOUS";
        public final String DEFAULT_EMAIL = DEFAULT_STRING + "@" + DEFAULT_STRING;
        public final String DEFAULT_NUMBER = "-1";
    }

    @UtilityClass
    public class RollbarConstants {
        public final String REQUEST_URL = "requestUrl";
        public final String REQUEST_METHOD = "requestMethod";
    }

    @UtilityClass
    public class PreAuthorize {
        public final String OR = " or ";
        public static final String ROLE_STUDENT = "hasRole('ROLE_STUDENT')";
        public static final String ROLE_ADMIN = "hasRole('ROLE_ADMIN')";
        public static final String ROLE_CAREER_ADMIN = "hasRole('ROLE_CAREER_ADMIN')";
        public static final String ROLE_ADMIN_OR_ROLE_CAREER_ADMIN = ROLE_CAREER_ADMIN + OR + ROLE_ADMIN;
    }

    @UtilityClass
    public class Kafka {
        public static final String EVENT_GROUP_PREFIX = "careers_task_";
    }

    @UtilityClass
    public class TaskCreationPlatform{
        public final String CRM = "CRM";
    }
}
