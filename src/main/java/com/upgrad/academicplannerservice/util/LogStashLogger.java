package com.upgrad.academicplannerservice.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@UtilityClass
public class LogStashLogger {
    /*
     * Only this class has been exposed to a specific ConsoleAppender. Needed for Error Logging in a defined format
     */
    public void log(String message) {
        log.info(message);
    }
}
