package com.upgrad.academicplannerservice.util;

import lombok.experimental.UtilityClass;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Collections;

@UtilityClass
public class HttpUtil {
    public HttpHeaders getHeaders(String authToken) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE));
        httpHeaders.setAccept(Collections.singletonList(MediaType.valueOf(MediaType.APPLICATION_JSON_VALUE)));
        httpHeaders.set(Constants.AUTH_TOKEN_HEADER, authToken);
        return httpHeaders;
    }
}
