package com.upgrad.academicplannerservice.auth.user;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class CentralUser {
    private String firstname;
    private String lastname;
    private String name;
    private String email;
    private Long id;
    private List<String> rolesStringSet;
}
