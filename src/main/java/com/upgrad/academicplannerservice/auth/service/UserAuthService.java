package com.upgrad.academicplannerservice.auth.service;


import com.upgrad.academicplannerservice.auth.user.CentralUser;

public interface UserAuthService {

    CentralUser getCurrentUser();
}
