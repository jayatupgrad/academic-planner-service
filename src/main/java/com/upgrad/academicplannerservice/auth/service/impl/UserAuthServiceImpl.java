package com.upgrad.academicplannerservice.auth.service.impl;


import com.upgrad.academicplannerservice.auth.service.UserAuthService;
import com.upgrad.academicplannerservice.auth.user.CentralAuthInfo;
import com.upgrad.academicplannerservice.auth.user.CentralUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UserAuthServiceImpl implements UserAuthService {
    @Override
    public CentralUser getCurrentUser() {
        return ((CentralAuthInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
    }
}
