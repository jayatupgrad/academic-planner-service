package com.upgrad.academicplannerservice.auth;

import com.upgrad.academicplannerservice.auth.user.CentralAuthInfo;
import com.upgrad.academicplannerservice.service.CentralService;
import com.upgrad.academicplannerservice.service.MDCService;
import com.upgrad.academicplannerservice.util.Constants;
import in.ueducation.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
public class TokenAuthenticationFilter extends OncePerRequestFilter {
    private final CentralService centralService;
    private final MDCService mdcService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader(Constants.AUTH_TOKEN_HEADER);
        mdcService.setAnonymousMDC();
        String rayID = mdcService.getRayId();
        response.setHeader("RayID", rayID);
        if (StringUtil.isNullOrEmpty(token)) {
            filterChain.doFilter(request, response);
            return;
        }
        Optional<CentralAuthInfo> optionalCentralAuthInfo = centralService.authenticateUser(token);
        if (optionalCentralAuthInfo.isPresent()) {
            CentralAuthInfo centralAuthInfo = optionalCentralAuthInfo.get();

            List<String> authorities = new ArrayList<>();

            for (String role : centralAuthInfo.getUser().getRolesStringSet())
                authorities.add(role);
            mdcService.setUserMDC(request, response, centralAuthInfo, authorities);
            UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                    centralAuthInfo, null, authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));

            SecurityContextHolder.getContext().setAuthentication(auth);
        }

        filterChain.doFilter(request, response);
    }
}
