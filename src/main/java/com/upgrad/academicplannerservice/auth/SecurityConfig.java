package com.upgrad.academicplannerservice.auth;

import com.upgrad.academicplannerservice.excp.ErrorConstants;
import com.upgrad.academicplannerservice.util.Constants;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.OPTIONS;

@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final TokenAuthenticationFilter tokenAuthenticationFilter;
    private final Environment environment;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // handle request authorization
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED, ErrorConstants.Auth.UNAUTHENTICATED_MESSAGE))
                .accessDeniedHandler((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_FORBIDDEN, ErrorConstants.Auth.UNAUTHORISED_MESSAGE))
                .and()
                // Add a filter to validate the auth-token with every request
                .addFilterBefore(tokenAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                // authorization requests config
                .authorizeRequests()
                .antMatchers(GET, "/health/**").permitAll()
                .antMatchers(GET,  "/live").permitAll()
                .antMatchers(OPTIONS).permitAll();
        Arrays.stream(environment.getActiveProfiles())
                .filter(profile -> !Constants.Profile.PRODUCTION.equals(profile))
                .findFirst()
                .ifPresent(nonProdProfile -> allowSwaggerUrls(http));

        // because we cant set antMatchers after call to anyRequest() is made
        http.authorizeRequests()
                .anyRequest()
                .authenticated();
    }

    @SneakyThrows
    private void allowSwaggerUrls(HttpSecurity http) {
        http.authorizeRequests()
                .antMatchers("/swagger-ui.html").permitAll()
                .antMatchers("/api-docs.json").permitAll()
                .antMatchers("/api-docs.json/**").permitAll()
                .antMatchers("/api-docs/swagger-config").permitAll()
                .antMatchers(GET, "/swagger-ui/*").permitAll()
                .antMatchers(GET, "/api-docs").permitAll();
    }
}
