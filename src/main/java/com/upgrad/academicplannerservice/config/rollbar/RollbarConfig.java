package com.upgrad.academicplannerservice.config.rollbar;

import com.rollbar.notifier.Rollbar;
import com.rollbar.notifier.config.Config;
import com.rollbar.spring.webmvc.RollbarSpringConfigBuilder;
import com.rollbar.web.provider.RequestProvider;
import com.upgrad.academicplannerservice.config.AcademicPlannerProperties;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@AllArgsConstructor
public class RollbarConfig {

  private final AcademicPlannerProperties academicPlannerProperties;
  private final Environment environment;

  @Bean
  public Rollbar rollbar() {
    return new Rollbar(getRollbarConfigs());
  }

  private Config getRollbarConfigs() {
    RequestProvider requestProvider = new RequestProvider
                    .Builder()
                    .userIpHeaderName("RayID")
                    .build();
    return RollbarSpringConfigBuilder.withAccessToken(academicPlannerProperties.getRollbarConfig().getRollbarToken())
            .environment(environment.getActiveProfiles()[0])
            .request(requestProvider)
            .person(new RollbarPersonProvider())
            .enabled(true)
            .build();
  }
}