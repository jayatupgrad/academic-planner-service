package com.upgrad.academicplannerservice.config.rollbar;

import com.rollbar.api.payload.data.Person;
import com.rollbar.notifier.provider.Provider;
import com.rollbar.web.listener.RollbarRequestListener;
import com.upgrad.academicplannerservice.auth.user.CentralAuthInfo;
import com.upgrad.academicplannerservice.auth.user.CentralUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class RollbarPersonProvider implements Provider<Person> {

    @Override
    public Person provide() {
      HttpServletRequest request = RollbarRequestListener.getServletRequest();
      CentralUser user = ((CentralAuthInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUser();
      if(user == null){
        return null;
      }
      String id = String.format("%d",user.getId());
      return new Person.Builder()
        .email(user.getEmail())
        .id(id)
        .build();
    }
  }