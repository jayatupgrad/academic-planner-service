package com.upgrad.academicplannerservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "academic-planner")
@Data
public class AcademicPlannerProperties {

    private final CentralServiceConfig centralServiceConfig = new CentralServiceConfig();
    private final RollbarConfig rollbarConfig = new RollbarConfig();
    private final AsyncExecutor asyncExecutor = new AsyncExecutor();
    private final KafkaConfig kafkaConfig = new KafkaConfig();

    @Data
    public static class CentralServiceConfig {
        private String baseUrl;
        private String authUrl;
    }

    @Data
    public static class RollbarConfig{
        private String rollbarToken;
    }

    @Data
    public static class AsyncExecutor {
        private int corePoolSize;
        private int maxPoolSize;
        private int queueCapacity;
    }

    @Data
    public static class KafkaConfig{
        private String bootStrapAddress;
        private String taskCreateUpdateTopic;
        private String taskStatusUpdateTopic;
        private String taskAttendanceTopic;
        private String taskFeedbackTopic;
    }
}
