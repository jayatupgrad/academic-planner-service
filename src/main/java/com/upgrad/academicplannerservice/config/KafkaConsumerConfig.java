package com.upgrad.academicplannerservice.config;

import com.upgrad.academicplannerservice.model.TaskCreateFromKafkaEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.ErrorHandler;
import org.springframework.kafka.listener.KafkaListenerErrorHandler;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumerConfig {

    private final AcademicPlannerProperties academicPlannerProperties;

    private Map<String, Object> getKafkaConsumerProps(){
        Map<String, Object> props = new HashMap<>();
        props.put(
                ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                academicPlannerProperties.getKafkaConfig().getBootStrapAddress());
        props.put(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class);
        props.put(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                JsonDeserializer.class);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        return props;
    }

    @Bean
    public ConsumerFactory<String, TaskCreateFromKafkaEvent> taskCreateFromKafkaEventConsumerFactory() {
        Map<String, Object> props = getKafkaConsumerProps();
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "planner_task");
        return new DefaultKafkaConsumerFactory<>(
                props, new StringDeserializer(),
                new JsonDeserializer<>(TaskCreateFromKafkaEvent.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, TaskCreateFromKafkaEvent> taskCreateFromKafkaEventConcurrentKafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, TaskCreateFromKafkaEvent> factory =
                new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(taskCreateFromKafkaEventConsumerFactory());
        factory.setErrorHandler(new ErrorHandler() {
            @Override
            public void handle(Exception thrownException, ConsumerRecord<?, ?> data) {

            }
        });
        factory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL_IMMEDIATE);
        return factory;
    }

    @Bean
    public KafkaListenerErrorHandler validationErrorHandler() {
        return (m, e) -> {
            log.error(e.getMessage());
            log.error(m.toString());
            long offset = (long) m.getHeaders().get("kafka_offset");
            int partition = (int) m.getHeaders().get("kafka_receivedPartitionId");
            String topic = (String) m.getHeaders().get("kafka_receivedTopic");
            Consumer consumer = (Consumer) m.getHeaders().get("kafka_consumer");
            Acknowledgment acknowledgment = (Acknowledgment) m.getHeaders().get("kafka_acknowledgment");
            TopicPartition topicPartition = new TopicPartition(topic, (int) partition);
            consumer.seek(topicPartition, offset + 1);
            consumer.commitAsync();
            //rollbarService.sendToRollbar(e);
            return null;
        };
    }

}
