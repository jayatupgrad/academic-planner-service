package com.upgrad.academicplannerservice.controller;

import com.upgrad.academicplannerservice.model.PlannerRequest;
import com.upgrad.academicplannerservice.model.PlannerResponse;
import com.upgrad.academicplannerservice.model.TaskRequest;
import com.upgrad.academicplannerservice.model.TaskCountResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class PlannerController {

    @PostMapping("/calendar/task")
    @Operation(summary = "Get All upcoming tasks")
    public List<PlannerResponse> getRegularTask(@Valid @RequestBody PlannerRequest plannerRequest) {
        // query DB using the request parm,
        // call service class to generate the response and return.
        return List.of(new PlannerResponse());
    }

    @PostMapping("/calendar/overdueTask")
    @Operation(summary = "Get All Overdue tasks")
    public List<PlannerResponse> getOverDueTask(@Valid @RequestBody TaskRequest taskRequest) {
        // query DB using the request parm,
        // call service class to generate the response and return.
        // only pick submissions and modules.
        return List.of(new PlannerResponse());
    }

    @PostMapping("/calendar/taskCount")
    @Operation(summary = "Get count of all task")
    public TaskCountResponse getTaskCount(@Valid @RequestBody TaskRequest taskRequest) {
        // query DB using the request parm,
        // call service class to generate the response and return.
        return new TaskCountResponse();
    }
}
/*
    @PostMapping("/calendar/update_state/")
    @Operation(summary = "update recent state of the task")
    public PlannerUpdateResponse updateTaskState(@Valid @RequestBody TaskUpdateStateFromKafkaEvent taskUpdateState) {
        // query DB using the request parm & update,
        // call service class to generate the response and return.
        return new PlannerUpdateResponse();
    }

    @PostMapping("/calendar/update_dueDate/")
    @Operation(summary = "update due Date of the task")
    public PlannerUpdateResponse updateTaskDueDate(@Valid @RequestBody TaskUpdateDueDateFromKafkaEvent taskUpdateDueDate) {
        // query DB using the request parm & update,
        // call service class to generate the response and return.
        return new PlannerUpdateResponse();
    }
}
*/