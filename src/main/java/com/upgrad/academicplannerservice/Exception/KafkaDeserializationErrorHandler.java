package com.upgrad.academicplannerservice.Exception;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.listener.ErrorHandler;
import org.springframework.kafka.listener.MessageListenerContainer;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class KafkaDeserializationErrorHandler implements ErrorHandler {

    //private final RollbarService rollbarService;

    @Override
    public void handle(Exception thrownException, List<ConsumerRecord<?, ?>> records, Consumer<?, ?> consumer, MessageListenerContainer container) {
        log.error("Error in process with Exception {} and the records are {}", thrownException, records);
       // rollbarService.sendToRollbar(thrownException);
        String s = thrownException.getMessage().split("Error deserializing key/value for partition ")[1].split(". If needed, please seek past the record to continue consumption.")[0];
        String topics = s.split("-")[0];
        int offset = Integer.valueOf(s.split("offset ")[1]);
        int partition = Integer.valueOf(s.split("-")[1].split(" at")[0]);
        TopicPartition topicPartition = new TopicPartition(topics, partition);
        consumer.seek(topicPartition, offset + 1);
        consumer.commitAsync();
    }

    @Override
    public void handle(Exception thrownException, ConsumerRecord<?, ?> data) {
    }
}
