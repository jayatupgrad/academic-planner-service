package com.upgrad.academicplannerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcademicPlannerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AcademicPlannerServiceApplication.class, args);
	}

}
