package com.upgrad.academicplannerservice.model;

public enum State {
    YET_TO_START,
    IN_PROGRESS,
    COMPLETED,
    EXPIRED,
    CANCELLED
}
