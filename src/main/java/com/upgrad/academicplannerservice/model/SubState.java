package com.upgrad.academicplannerservice.model;

public enum SubState {
    MODULE_LOCKED,
    MODULE_UNLOCKED,
    PENDING,
    DONE,
    RECORDING_AVAILABLE,
    RECORDING_UNAVAILABLE,
    FEEDBACK_AVAILABLE,
    FEEDBACK_UNAVAILABLE
}
