package com.upgrad.academicplannerservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class PlannerResponse {

    String title;

    String subTitle;

    String taskRemoteId;

    String eventType;

    Long durationInMinutes;

    Date dueDate;

    String link;

    Long cohortId;

    Long userId;

    String ctaText;

    String primaryCta;

    String secondaryCta;

    boolean primaryCtaFlag;

    boolean secondaryCtaFlag;

    String stateDisplayText;

    String agendaDisplayText;

    Category category;

    boolean isMandatory;
}
