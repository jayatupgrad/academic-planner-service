package com.upgrad.academicplannerservice.model;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TaskUpdateDueDateFromKafkaEvent {

    @NotNull
    String taskRemoteId;

    @NotEmpty
    List<Long> userID;

    @NotNull
    Long cohortId;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    Date dueDate;
}
