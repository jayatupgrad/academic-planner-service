package com.upgrad.academicplannerservice.model;

public enum EventType {
    MODULE_DEADINE("MODULE DEADINE"),
    MODULE_SUBMISSION("MODULE SUBMISSION"),
    LIVE_SESSION("LIVE SESSION"),
    SGC("SGC"),
    LEARNER_PROFILE("LEARNER PROFILE");

    private String eventTypeData;

    EventType(String eventTypeData) {
        this.eventTypeData = eventTypeData;
    }
}
