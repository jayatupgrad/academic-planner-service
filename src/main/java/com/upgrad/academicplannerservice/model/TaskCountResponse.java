package com.upgrad.academicplannerservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskCountResponse {
    int submissions;
    int live;
    int module;
    int career_outcome;
}
