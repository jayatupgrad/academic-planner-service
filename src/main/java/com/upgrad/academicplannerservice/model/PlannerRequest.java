package com.upgrad.academicplannerservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class PlannerRequest {

    @NotNull
    String cohortId;

    List<Category> categories;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    Date FromDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    Date ToDate;

}
