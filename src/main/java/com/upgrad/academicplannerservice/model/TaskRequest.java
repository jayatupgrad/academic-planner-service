package com.upgrad.academicplannerservice.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class TaskRequest {
    @NotNull
    String cohortId;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    Date FromDate;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    Date ToDate;
}
