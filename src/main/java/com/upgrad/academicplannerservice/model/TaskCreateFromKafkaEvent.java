package com.upgrad.academicplannerservice.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@SuperBuilder
public class TaskCreateFromKafkaEvent {
    @NotNull
    String title;

    @NotNull
    String subTitle;

    @NotNull
    String taskRemoteId;

    @NotNull
    EventType eventType;

    @NotNull
    Long durationInMinutes;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    Date dueDate;

    @NotNull
    String link;

    @NotNull
    Long cohortId;

    @NotNull
    List<Long> userId;

    @NotNull
    State stateDisplayData;

    @NotNull
    SubState subState;

    String ctaText;

    String agendaDisplayText;

    Category category;

    boolean isMandatory;
}
