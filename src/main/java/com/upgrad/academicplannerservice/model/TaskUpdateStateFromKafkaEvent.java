package com.upgrad.academicplannerservice.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class TaskUpdateStateFromKafkaEvent {

    @NotNull
    String taskRemoteId;

    @NotEmpty
    List<Long> userID;

    @NotNull
    String cohortId;

    State state;

    SubState subState;

    String ctaText;
}
