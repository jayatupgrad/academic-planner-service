package com.upgrad.academicplannerservice.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PlannerUpdateResponse {

    int status;

    String message;
}
