package com.upgrad.academicplannerservice.model;

public enum Category {
    SUBMISSION,
    LIVE,
    MODULE,
    CAREER
}
