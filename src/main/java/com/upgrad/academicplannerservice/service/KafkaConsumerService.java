package com.upgrad.academicplannerservice.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
@Getter
public class KafkaConsumerService {

    @KafkaListener(topics = "mytopic",
            groupId = "create_update_task", clientIdPrefix = "career_task_create_update",
            containerFactory = "taskCreateFromKafkaEventConcurrentKafkaListenerContainerFactory",
            errorHandler = "validationErrorHandler")
    public void listenWithHeaders(
            @Payload String message,
            @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
        System.out.println(
                "Received Message: " + message + "from partition: " + partition);
    }
}
