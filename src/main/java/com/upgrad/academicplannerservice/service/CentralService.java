package com.upgrad.academicplannerservice.service;

import com.upgrad.academicplannerservice.auth.user.CentralAuthInfo;

import java.util.Optional;

public interface CentralService {
    Optional<CentralAuthInfo> authenticateUser(String authToken);
}
