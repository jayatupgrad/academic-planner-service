package com.upgrad.academicplannerservice.service;

import javax.servlet.http.HttpServletRequest;

public interface RollbarService {

    void sendToRollbar(Exception exception);

    void sendToRollbar(Exception exception, String data);

    void sendToRollbar(Exception exception, HttpServletRequest httpServletRequest);
}
