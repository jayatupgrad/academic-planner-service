package com.upgrad.academicplannerservice.service;

import com.upgrad.academicplannerservice.auth.user.CentralAuthInfo;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface MDCService {
    void setAnonymousMDC();
    String getRayId();
    void setUserMDC(HttpServletRequest request, HttpServletResponse response, CentralAuthInfo centralAuthInfo, List<String> authorities);
}
