package com.upgrad.academicplannerservice.service.impl;


import com.upgrad.academicplannerservice.auth.user.CentralAuthInfo;
import com.upgrad.academicplannerservice.service.MDCService;
import com.upgrad.academicplannerservice.util.Constants;
import in.ueducation.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import eu.bitwalker.useragentutils.UserAgent;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class MDCServiceImpl implements MDCService {
    private static final String UNKNOWN = "unknown";

    @Override
    public String getRayId(){
        return MDC.get("RayID");
    }

    @Override
    public void setAnonymousMDC() {
        String rayId = UUID.randomUUID().toString();
        MDC.put("RayID", rayId);
        MDC.put(Constants.MDCConstants.USER_ID, Constants.MDCConstants.DEFAULT_NUMBER);
        MDC.put(Constants.MDCConstants.EMAIL, Constants.MDCConstants.DEFAULT_EMAIL);
        MDC.put(Constants.MDCConstants.ROLE, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.PLATFORM, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.DEVICE_TYPE, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.OPERATING_SYSTEM, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.BROWSER_INFO, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.REQUEST_TYPE, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.REQUEST_URL, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.IP_ADDRESS, Constants.MDCConstants.DEFAULT_STRING);
        MDC.put(Constants.MDCConstants.TOTAL_TIME, "notSet");
        MDC.put(Constants.MDCConstants.REQUEST_BODY, "notSet");
        MDC.put(Constants.MDCConstants.ERROR_MESSAGE, "notSet");
    }

    @Override
    public void setUserMDC(HttpServletRequest request, HttpServletResponse response, CentralAuthInfo centralAuthInfo, List<String> authorities) {
        MDC.put(Constants.MDCConstants.EMAIL, centralAuthInfo.getUser().getEmail());
        MDC.put(Constants.MDCConstants.USER_ID, String.valueOf(centralAuthInfo.getUser().getId()));
        MDC.put(Constants.MDCConstants.REQUEST_URL, getFullURL(request));
        MDC.put(Constants.MDCConstants.REQUEST_TYPE, request.getMethod());
        MDC.put(Constants.MDCConstants.AUTH_TOKEN, request.getHeader(Constants.AUTH_TOKEN_HEADER));

        String role = "MISSING";
        if (!authorities.isEmpty()) {
            role = authorities.toString().replace(" ", "");
        }
        MDC.put(Constants.MDCConstants.ROLE, role);

        String sessionID = request.getHeader(Constants.MDCConstants.SESSION_ID);
        if (StringUtil.isNullOrEmpty(sessionID)) {
            sessionID = "MISSING";
        }
        MDC.put(Constants.MDCConstants.SESSION_ID, sessionID);

        setIPAddress(request);
        setMetaInformation(request);
    }

    private void setIPAddress(HttpServletRequest request) {
        String ipAddress = request.getHeader(Constants.MDCConstants.HEADER_CLIENT_IP);
        if (StringUtil.isNullOrEmpty(ipAddress)) {
            ipAddress = request.getRemoteAddr();
        }
        if ("0:0:0:0:0:0:0:1".equals(ipAddress)) {
            MDC.put(Constants.MDCConstants.IP_ADDRESS, "127.0.0.1");
        } else {
            MDC.put(Constants.MDCConstants.IP_ADDRESS, ipAddress.split(",")[0].replace(" ", ""));
        }
    }

    private String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();

        if (queryString != null) {
            return requestURL.append('?').append(queryString).toString();
        } else {
            return requestURL.toString();
        }
    }

    private void setMetaInformation(HttpServletRequest request) {
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader(Constants.MDCConstants.HEADER_USER_AGENT));

        setDeviceType(request, userAgent);
        setOperatingSystemAndPlatform(request, userAgent);
        setBrowserInfo(request, userAgent);
    }

    private void setDeviceType(HttpServletRequest request, UserAgent userAgent) {
        String deviceType = request.getHeader(Constants.MDCConstants.DEVICE_TYPE);
        if (StringUtil.isNullOrEmpty(deviceType)) {
            try {
                deviceType = userAgent.getOperatingSystem().getDeviceType().getName();
            } catch (Exception e) {
                deviceType = Constants.MDCConstants.DEFAULT_STRING;
            }
        }
        if (deviceType.toLowerCase().contains(UNKNOWN)) {
            deviceType = Constants.MDCConstants.DEFAULT_STRING;
        }
        MDC.put(Constants.MDCConstants.DEVICE_TYPE, deviceType.replace(" ", ""));
    }

    private void setOperatingSystemAndPlatform(HttpServletRequest request, UserAgent userAgent) {
        String operatingSystem = request.getHeader(Constants.MDCConstants.OPERATING_SYSTEM);
        if (StringUtil.isNullOrEmpty(operatingSystem)) {
            try {
                operatingSystem = userAgent.getOperatingSystem().getName();
            } catch (Exception e) {
                operatingSystem = Constants.MDCConstants.DEFAULT_STRING;
            }
        }
        if (operatingSystem.toLowerCase().contains(UNKNOWN)) {
            operatingSystem = Constants.MDCConstants.DEFAULT_STRING;
        }
        MDC.put(Constants.MDCConstants.OPERATING_SYSTEM, operatingSystem.replace(" ", ""));

        String platform = request.getHeader(Constants.MDCConstants.PLATFORM);
        operatingSystem = operatingSystem.toLowerCase();
        if (StringUtil.isNullOrEmpty(platform)) {
            if (operatingSystem.contains("android")) {
                platform = "Android";
            } else if (operatingSystem.contains("ios")) {
                platform = "iOS";
            } else if (operatingSystem.contains(Constants.MDCConstants.DEFAULT_STRING.toLowerCase())) {
                platform = Constants.MDCConstants.DEFAULT_STRING;
            } else {
                platform = "Web";
            }
        }
        MDC.put(Constants.MDCConstants.PLATFORM, platform.replace(" ", ""));
    }

    private void setBrowserInfo(HttpServletRequest request, UserAgent userAgent) {
        String browserInfo = request.getHeader(Constants.MDCConstants.BROWSER_INFO);
        if (StringUtil.isNullOrEmpty(browserInfo)) {
            try {
                browserInfo = (userAgent.getBrowser().getName() + userAgent.getBrowserVersion().getVersion());
            } catch (Exception e) {
                browserInfo = Constants.MDCConstants.DEFAULT_STRING;
            }
        }
        if (browserInfo.toLowerCase().contains(UNKNOWN)) {
            browserInfo = Constants.MDCConstants.DEFAULT_STRING;
        }
        MDC.put(Constants.MDCConstants.BROWSER_INFO, browserInfo.replace(" ", ""));
    }
}

