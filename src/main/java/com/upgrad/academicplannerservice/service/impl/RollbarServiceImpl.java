package com.upgrad.academicplannerservice.service.impl;

import com.google.common.collect.ImmutableList;
import com.rollbar.notifier.Rollbar;
import com.upgrad.academicplannerservice.service.RollbarService;
import com.upgrad.academicplannerservice.util.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class RollbarServiceImpl implements RollbarService {

    private static final List<String> ROLLBAR_BANNED_PROFILES = ImmutableList.of(Constants.Profile.LOCAL, Constants.Profile.TEST);
    private final Rollbar rollbar;

    @Override
    public void sendToRollbar(Exception exception) {
        log(() -> rollbar.error(exception));
    }

    @Override
    public void sendToRollbar(Exception exception, String data) {
        log(() -> rollbar.error(exception, data));
    }

    @Override
    public void sendToRollbar(Exception exception, HttpServletRequest httpServletRequest) {
        log(() -> rollbar.error(exception, getDataFromServletRequest(httpServletRequest)));
    }

    private void log(Runnable runnable) {
        boolean skip = ROLLBAR_BANNED_PROFILES.contains(rollbar.config().environment());
        if (! skip) {
            runnable.run();
        }
    }

    private Map<String, Object> getDataFromServletRequest(HttpServletRequest httpServletRequest) {
        Map<String, Object> mapData = new LinkedHashMap<>();
        mapData.put(Constants.RollbarConstants.REQUEST_URL, httpServletRequest.getRequestURI());
        mapData.put(Constants.RollbarConstants.REQUEST_METHOD, httpServletRequest.getMethod());
        CollectionUtils.toIterator(httpServletRequest.getHeaderNames()).forEachRemaining(headerName -> mapData.put(headerName, httpServletRequest.getHeader(headerName)));
        CollectionUtils.toIterator(httpServletRequest.getParameterNames()).forEachRemaining(parameterName -> mapData.put(parameterName, httpServletRequest.getParameter(parameterName)));
        return mapData;
    }
}
