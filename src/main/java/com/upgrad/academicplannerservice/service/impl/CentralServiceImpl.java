package com.upgrad.academicplannerservice.service.impl;


import com.upgrad.academicplannerservice.auth.user.CentralAuthInfo;
import com.upgrad.academicplannerservice.config.AcademicPlannerProperties;
import com.upgrad.academicplannerservice.service.CentralService;
import com.upgrad.academicplannerservice.util.HttpUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class CentralServiceImpl implements CentralService {
    private final RestTemplate restTemplate;
    private final AcademicPlannerProperties academicPlannerProperties;

    @Override
    public Optional<CentralAuthInfo> authenticateUser(String authToken) {
        String centralAuthUrl = academicPlannerProperties.getCentralServiceConfig().getBaseUrl() + academicPlannerProperties.getCentralServiceConfig().getAuthUrl();
        try {
            ResponseEntity<CentralAuthInfo> authResponse = restTemplate.exchange(centralAuthUrl, HttpMethod.GET, new HttpEntity<>(HttpUtil.getHeaders(authToken)), CentralAuthInfo.class);
            return Optional.of(authResponse.getBody());
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage(), e);
            log.error("Response body for authentication {}", e.getResponseBodyAsString());
            return Optional.empty();
        }
    }
}

