package com.upgrad.academicplannerservice.excp;

import lombok.experimental.UtilityClass;

import java.util.function.BiFunction;
import java.util.function.Function;

@UtilityClass
public class ErrorConstants {

    public final BiFunction<String, String, String> RESOURCE_WITH_ID_NOT_FOUND = (resource, id) -> String.format("%s with id %s not found.", resource, id);
    public final BiFunction<String, String, String> RESOURCE_WITH_REMOTE_ID_AND_SOURCE_NOT_FOUND = (resource, source_remote_id) -> String.format("%s with source_remote_id %s not found.", resource, source_remote_id);
    public final Function<String, String> RESOURCE_NOT_FOUND = (resource) -> String.format("%s not found.", resource);
    public final BiFunction<String, Integer, String> RESOURCE_LIMIT_EXCEEDED = (resource, limit) -> String.format("%s limit exceeded in session. Can add only %d members.", resource, limit);
    public final Function<String, String> ACCESS_DENIED = (resource) -> String.format("%s not allowed to perform this action", resource);
    public final Function<String, String> UNAUTHORISED = (resource) -> String.format("%s cannot perform this action", resource);


    @UtilityClass
    public class Role {
        public final String ROLE_TYPE_REQUIRED = "Role Type is required to get role object.";
    }


    @UtilityClass
    public class Auth {
        public final String UNAUTHENTICATED_MESSAGE = "Request is unauthenticated";
        public final String UNAUTHORISED_MESSAGE = "Access Denied";
    }

}

